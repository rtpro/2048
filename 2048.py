#!/usr/bin/python

def handleRow(row,direction):
    print row
    # Remove all zeros, keep count of zeros to add them later
    zeros = removeZeros(row)

    # Reverse order based on selected direction
    if (direction=="right"):
        row.reverse()

    # Merge *once* on accurance of same digit
    for cell in xrange(0,len(row)-1):
        if (row[cell] == row[cell+1]):
            row[cell] = row[cell]*2
            row[cell+1] = 0
            zeros = zeros + 1
            row.remove(0)
            break

    # Add finising zeros
    addZeros(row,zeros)

    # Reverese back for right direction
    if (direction=="right"):
        row.reverse()

    print row
    print ""

def removeZeros(row):
#returns ZerosCount
    zeroCount=0
    while 0 in row:
        zeroCount=zeroCount+1
        row.remove(0)
    return zeroCount

def addZeros(row,count):
    for i in range(0,count):
        row.insert(len(row),0)

def main():

    handleRow([2,0,0,2],"left")
    handleRow([2,0,0,2],"right")

    handleRow([2,0,2,2],"left")
    handleRow([2,0,2,2],"right")

    handleRow([2,4,2,2],"left")
    handleRow([2,4,2,2],"right")

    handleRow([2,4,8,2],"left")
    handleRow([2,4,8,2],"right")

if __name__ == "__main__":
    main()
